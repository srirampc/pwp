---
title: About QSAC
---

# About QSAC

_QSAC_ is a made-up acronym for _quaerere scientiam apparatus ut
computet_, which is a latin translation of the phrase
_seeking the knowledge of the machine that computes_ according to Google
Translate - as done in early 2011. Google Translate has since
been updated and no longer generates this latin phrase, but I would
still like to keep it that way. I am not sure if the translation is
a correct, but I do find "que-sack" catchy.

[I][] started this journal in [February 2011][start] shortly after I
began my current position as a research scholar in the [Computer
Science Department][cse] in [IIT Bombay][iitb]. 

[I]: /
[start]: /blog/2011/02/14/introducing_qsac.html
[cse]: http://www.cse.iitb.ac.in/
[iitb]: http://www.iitb.ac.in/


