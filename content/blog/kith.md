---
title: Kith
---

Resources
=========

>**kith** , _n_:
>  1. Knowledge, acquaintance with something; knowledge communicated,
>  information.
>  2. The persons who are known or familiar, taken collectively; one's friends,
>  fellow-countrymen, or neighbours; acquaintance;

This page is a collection of [blogs](#blogs), [bookmarks](#bookmarks) and
[papers](#papers) and services I use to keep tabs on what is happening in
machine learning research.

Blogs
-----

The is a list of blogs that I find interesting. I will
add as I find newer ones.

* [RNA-Seq Blog](http://www.rna-seqblog.com/)
* [Titus Brown's Blog](http://ivory.idyll.org/blog/)
* [Ethan Perlstein's Lab](http://perlsteinlab.com/#filter=.blog)
* [The Glowing Python](http://glowingpython.blogspot.in/)

Papers
------

I keep track of papers to read using the
[CiteULike](http://www.citeulike.org/user/srirampc) service.


Bookmarks
---------

I collect  bookmarks using the
[delicious](http://delicious.com/srirampcl) service.
