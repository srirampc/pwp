---
title: Grades of Spring Semester 2010-11
tags: [academic]
created_at: 2011/05/10
kind: article
excerpt: 'Grades : Oven and Freezer.'
disqus: true
---

I had three courses for credit with my grades 6.0,9.0 and 10.0, and
that brings my CPI to a safe 8.3, well above the required 7.5. Even
though the grades are very disproportional to the amount of work I had
done. I can make case that its inversely proportional. As I learned from
[Prof. Uday Khedkar](http://www.cse.iitb.ac.in/~uday/)'s talk on
research, it probably is true that "Grades on courses reflect relative
stupidity". 
Looking at CPI of 8.3 with grades ranging from 6.0 to 10.0, I am
reminded of this saying
>  If your head is in the freezer and your feet are in the oven, 
>  on average you're comfortable.

That was a comfortable semester on an average.

