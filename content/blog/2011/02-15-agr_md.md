---
title: Dr. AGR M.D.
tags: [teaching]
created_at: 2011/02/15
kind: article
excerpt: Few observations on my favorite teacher AGR
disqus: true
---

There are two kinds of doctors - the surgeon and the physician.
My father is a doctor - specifically an orthopedic
surgeon. While the surgeon's task is to cut it open, fix and stitch, the
physician studies many diagnostic reports to identify the cause of
the reported symptoms. My dad - as a typical surgeon - scoffs the
physicians, saying that they don't have the guts to open up a body,
and hence choose to be on the sidelines writing prescriptions. 

The primary skill required for the surgeon and the physician are
different. The surgeon needs the hands of a craftsman, while the
physician needs the wits of a detective. The surgeon needs to have 
the nerve of a professional athlete of an individual sport (such as
Golf), while the physician needs the management skills of a 
coach or captain (You knew, I was going for the sports metaphor). I
know a few qualified high school students, who choose Engineering
over Medicine unaware of this dichotomy - making the choice only based
on the concerns over the skills required for surgery.

As people who are in search of knowledge about computing machines
(irrespective of whether the machines are abstract or actual), I
think, we are more like physicians than surgeons. Of the 
computer-scientists I have seen, the one  with the most
physician-skill (in the metaphorical sense) is 
[AGR](http://www.cse.iitb.ac.in/~ranade). I say that based on 
sitting in his Graph Theory class this semester. When a question is
posed in the class, AGR gets to the diagnosis within a moment's time
- much faster than the time it takes for me to understand the symptom of
misunderstanding.

Most of the time, a physician has to imagine the structure of the
problem relying on unreliable evidence. With advances in diagnostic
technologies, there are many ways - CAT scan, MRI etc -  where a physical picture
of the problem can be learned without difficulty. In the class room, AGR always
has a picture of what he is talking about - right there on
the black-board.When a difficult question comes up, the first thing he
says : "Lets look at the picture" (or) "Let me draw a different
picture". He never starts a topic without a picture. If there is one
thing, I will take away from his lectures : "Keep the picture on board
for every one to see, before you talk about the problem".

In this semester in IIT Bombay,  I
have two other courses - 'Machine Learning', and 'Design and Analysis of
Algorithms', apart from Graph Theory. All of them are taught by amazing
people - much better than almost any teacher in my previous seven
years of academic learning. Of the three, AGR has a distinct habit I
have come to respect. He first divides the black-board into columns,
starts on the unwritten left column of the black-board and fills
column by column, as the narrative develops, towards the right. As
simple as this habit may seem, it shows 
the rigor and discipline on par with how an experienced physician goes
about to work his diagnosis.

Of course, AGR has the skill possessed by any good teacher :
development of a course-narrative. Apart from that, there are three
things which makes AGR's teaching much better than anyone I have
learned under : quick and improv. deductive skill, having a 2D/3D vector
representation of the problem, and black-board discipline. These
skills no different from a physician, and that makes AGR a great teacher. 


