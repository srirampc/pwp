---
title: Why I fail to solve problems ?
tags: [problem solving]
created_at: 2011/03/20
kind: article
excerpt: Observations on my problem solving failures.
disqus: true
---

The most difficult subject in this semester for me is 'Design and
Analysis of Algorithms'. I have been solving a number of problems with
the degree of difficulty much higher than what I have been solving
earlier in my under-graduate courses. I am failing more often than I
have been succeeding. When I fail, I find I fail for the following
reasons :

1. Focus on restricted lines of inquiry : When given a problem, I try to
   solve with one or two of the known lines of inquiry such as Proof of
   Induction or Proof by Contradiction using the obvious bases. But
   for many problems the bases for induction or the contradiction is
   not very obvious, and in many problems - they are not even the right lines
   of inquiry. So, what happens now is that I am boxed in with known
   lines of inquiry
2. Giving up too early : For some problems, a technique may not be
   look promising in the first instance. What I do is give up
   entirely, and follow some other technique and go no where. And once
   I give up something, it takes a lot of time and effort to get back
   to the given-up line of reasoning.
3. Not giving up early : In some cases, I follow a wrong lead and
   do not give up early enough. The trouble with the giving up vs. not
   giving up is that you never know what is applicable for the problem at
   hand. Should I give up and try something else ? (or) Should I keep
   going on this line of inquiry ? Some students are able identify it
   faster. Its takes a longer time for me.
4. Lack of Internal Feedback : When I observe those people who solve
   problems faster, I see that they have a sense of internal feedback,
   that strikes back when they have a wrong lead. This sense is
   quicker and faster for some. Some times, I stare at a problem for
   long time, with no feedback what so ever internally. Is there a way
   to develop such a skill ?

These are not necessarily the reasons why I am unable to solve the
problem. They are the places where I get stuck. When I see that some one
else in the class has found the solution, I just see where I am and it
is mostly one of the above! I do find that in many
cases I am able to eventually find a solution, only that it takes a
longer time for me than the better quarter or one-third of the class.

The most interesting thing about these reasons is that it is NEVER the
case that some one solved the problem using a technique that I have
never heard. It is always something I know, if I only I had thought
'HARD' enough! But is 'HARD' the right way to think ? Prof. Diwan says,
"Just practice!"
