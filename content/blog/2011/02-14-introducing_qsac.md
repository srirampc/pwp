---
title: Welcome to my blog
tags: [intro]
created_at: 2011/02/14
kind: article
excerpt: A short post describing what I intend for my blog.
disqus: true
---

Welcome to my blog on research life.

I'm a M.Tech graduate who has taken up a Ph.D.
position in the
[Computer Science and Engineering][cse] department at
[IIT,Bombay][iitb].

[cse]: http://www.cse.iitb.ac.in/
[iitb]: http://www.iitb.ac.in/

The plan here is to write about my research life, and to discuss
papers, questions or thoughts related to parallel computing, and systems
biology. I'll probably concentrate on my research interests,
including: parallel computing, Bayesian networks, and gene regulatory 
networks. On the softer side, I'll talk about learning, teaching,
reading, research and all things IIT Bombay. 

