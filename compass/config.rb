# -*- coding: utf-8 -*-
# This is the Compass configuration file. The configuration file for nanoc is
# named “config.yaml”.
require 'susy'

# project_path = File.dirname(__FILE__)
http_path = '/'
output_style = :compressed
sass_dir = 'content/assets/css'
css_dir = 'output/assets/css'
images_dir = '/assets/images'
http_images_path = 'output/assets/images'
javascripts_dir = 'content/assets/js'
http_javascripts_path = '/javascripts'
# extensions_dir = 'compass_extensions'

sass_options = {
  :syntax => :scss
}
